import React, {useState,useEffect} from 'react'
import "./styles.css";

import api from './services/api'

function App() {

  const [repositories,setRepositories] = useState([])

    useEffect(() => {
      api.get('repositories').then(response => {
        setRepositories(response.data)
      })
  }, [])
  
  
  async function handleAddRepository() {
    const response = await api.post("repositories",{
    title: "Desafio ",
    url: "https://github.com/rocketseat-education/bootcamp-gostack-desafios/tree/master/desafio-conceitos-nodejs",
    techs: [
      "Node.js",
      "React-Native",
      "ReactJs"
    ]
    })
    setRepositories([ ...repositories,response])
  }

  async function handleRemoveRepository(id) {
    await api.delete(`repositories/${id}`)

      setRepositories(repositories.filter(
        repository => repository.id != id
      ))
  }

  return (
    <div>
      <ul data-testid="repository-list">
        <h1>Repositorios</h1>
          {repositories.map(repository => ( 
          <li>
            <li key={repository.id}><h4>{repository.title}</h4>  { repository.url}</li>
          
            <button onClick={() => handleRemoveRepository(repository.id)}>
              Remover
            </button>
          </li>
        ))}
      <button onClick={handleAddRepository}>Adicionar</button>
      </ul>
    </div>
  );
}

export default App;
